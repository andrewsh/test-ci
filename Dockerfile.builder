FROM debian:testing-slim

ARG DEBIAN_FRONTEND=noninteractive

RUN echo 'deb-src https://deb.debian.org/debian unstable main contrib' > /etc/apt/sources.list.d/debian-sources.list

RUN apt-get update \
 && apt-get install -yq \
    sbuild schroot debootstrap postgresql-client python3 python3-yaml \
    reserialize jq \
    devscripts libhtml-format-perl python3-buildlog-consultant ntpsec

RUN sbuild-createchroot --include=eatmydata,ccache,gnupg unstable /srv/chroot/unstable-amd64-sbuild http://deb.debian.org/debian
