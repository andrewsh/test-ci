base="$PWD"

verify_status() {
	pkg="$1"
	bdep="$2"
	expected="$3"
	if [ -f "$base/status/$pkg.$bdep" ]
	then
		read status < "$base/status/$pkg.$bdep"
		[ "$status" = "$expected" ]
	fi
}

set_status() {
	pkg="$1"
	bdep="$2"
	status="$3"
	echo "$status" > "$base/status/$pkg.$bdep"
}

prepare_build() {
	mkdir -p "$base/out"
	cd "$base/out"
}

clean_build() {
	cd "$base"
	rm -rvf "$base/out"
}

sbuild() {
	echo RUNNING: sbuild "$@"
	eval command sbuild "$@"
}

version_config="$base/versions.yaml"

source_packages() {
	yaml2json < "$version_config" | jq -r 'keys | .[]'
}

binary_packages_for() {
	package="$1"
	yaml2json < "$version_config" | jq -r '.["'$package'"] | keys | .[]'
}

binary_package_version_for() {
	package="$1"
	binary_package="$2"
	yaml2json < "$version_config" | jq -r '.["'$package'"]["'$binary_package'"]'
}

extra_debs_for() {
	pkg="$1"

	for binpkg in $(binary_packages_for $pkg)
	do
		version=$(binary_package_version_for $pkg $binpkg)
		existing="$(echo $base/snapshots/$pkg/${binpkg}_${version}_*.deb)"
		if [ -f "$existing" ]
		then
			echo --extra-package="$(readlink -e "$existing")"
		fi
	done
}

bdeps_for() {
	pkg="$1"

	for binpkg in $(binary_packages_for $pkg)
	do
		version=$(binary_package_version_for $pkg $binpkg)
		echo "--add-depends=\""$binpkg" (= "$version")\""
	done
}
